package com.doranco.multitiers.ejb.interfaces;

import java.util.List;

import javax.ejb.Remote;

import com.doranco.multitiers.ejb.interfaces.exceptions.LibraryExceptions;
import com.doranco.multitiers.entity.User;
import com.doranco.multitiers.util.Page;

@Remote
public interface IUser {

	public User suscribe(User user) throws LibraryExceptions;

	public User connect(String userName, String passwrd) throws LibraryExceptions;

	public List<User> getAll() throws LibraryExceptions;

	public User qetUser(long id) throws LibraryExceptions;

	public Page<User> find(int pagesize, int pageNumber) throws LibraryExceptions;

}
