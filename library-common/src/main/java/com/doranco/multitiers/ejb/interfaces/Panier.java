
package com.doranco.multitiers.ejb.interfaces;
import java.util.List;

import javax.ejb.Remote;

import com.doranco.multitiers.ejb.interfaces.exceptions.LibraryExceptions;


@Remote
public interface Panier {
	public void addBook(String book) throws LibraryExceptions;

	public void removeBook(String book) throws LibraryExceptions;

	public List<String> getBook() throws LibraryExceptions;

}
