package com.doranco.multitiers.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "l_book")
public class Book extends Identifiant implements Serializable {

	private static final long serialVersionUID = 3643049575445100695L;

	private String isbn;
	private String title;

	@OneToMany(mappedBy = "book", fetch = FetchType.LAZY)
	private List<Note> notes;

	@OneToMany(mappedBy = "book", fetch = FetchType.LAZY)
	private List<Viewing> viewings;

	// Constructeur
	public Book() {
	}

	public List<Note> getNotes() {
		return notes;
	}

	public void setNotes(List<Note> notes) {
		this.notes = notes;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
