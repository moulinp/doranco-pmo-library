package com.doranco.multitiers.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "l_note")
@IdClass(IdNote.class)
public class Note implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8990184735936604309L;

	public Note() {
		super();
	}

	@Id
	@ManyToOne
	private User user;

	@Id
	@ManyToOne
	private Book book;

	private Date notreDate;
	private int value;
	private String comment;

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	public Date getNotreDate() {
		return notreDate;
	}

	public void setNotreDate(Date notreDate) {
		this.notreDate = notreDate;
	}

}
