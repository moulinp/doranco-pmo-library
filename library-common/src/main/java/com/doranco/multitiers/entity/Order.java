package com.doranco.multitiers.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
@Entity
@Table(name="l_order")
public class Order extends Identifiant implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Order() {
		super();
	}
	
	@ManyToOne
	private User user;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	
}
