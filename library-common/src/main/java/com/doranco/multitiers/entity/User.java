package com.doranco.multitiers.entity;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "l_user")
public class User extends Identifiant implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5529889845625514357L;

	private String firstName;
	private String lastName;
	private String userName;
	
	private String passwrd;
	private boolean isadmin = false;

	@OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
	private Set<Order> order;
	@OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
	private List<Note> notes;

	// Constructeur
	public User() {
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPasswrd() {
		return passwrd;
	}

	public void setPasswrd(String passwrd) {
		this.passwrd = passwrd;
	}

	public boolean isIsadmin() {
		return isadmin;
	}

	public void setIsadmin(boolean isadmin) {
		this.isadmin = isadmin;
	}

	public List<Note> getNotes() {
		return notes;
	}

	public void setNotes(List<Note> notes) {
		this.notes = notes;
	}

	public Set<Order> getOrder() {
		return order;
	}

	public void setOrder(Set<Order> order) {
		this.order = order;
	}

}
