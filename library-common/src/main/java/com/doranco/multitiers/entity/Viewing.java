package com.doranco.multitiers.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "l_viewing")
@IdClass(IdView.class)
public class Viewing implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private IdView idView = new IdView();

	private Date startDate;
	private long duration;

	@Id
	@ManyToOne
	private User user;

	@Id
	@ManyToOne
	private Book book;

	public Viewing() {
		super();
		// TODO Auto-generated constructor stub
	}

	public IdView getIdView() {
		return idView;
	}

	public void setIdView(IdView idView) {
		this.idView = idView;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public long getDuration() {
		return duration;
	}

	public void setDuration(long duration) {
		this.duration = duration;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

}
