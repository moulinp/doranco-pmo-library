package com.doranco.multitiers.util;

import java.io.Serializable;
import java.util.List;

/*
 * class permettant d'afficher les pages suivant  des criteres specifiques
 */
public class Page<T> implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1914220831787642100L;
	private int PageSize;
	private int pageNumber;
	private long totalCount;
	private List<T> content;

	// constructeur
	public Page() {
		super();
		// TODO Auto-generated constructor stub

	}

	public int getPageSize() {
		return PageSize;
	}

	public void setPageSize(int pageSize) {
		PageSize = pageSize;
	}

	public int getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}

	public long getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(long totalCount) {
		this.totalCount = totalCount;
	}

	public List<T> getContent() {
		return content;
	}

	public void setContent(List<T> content) {
		this.content = content;
	}

}
