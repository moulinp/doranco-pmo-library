package com.doranco.multitiers.dao.test;


import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.util.List;

import javax.ejb.EJB;

import org.apache.log4j.Logger;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.doranco.multitiers.dao.GenericDAO;
import com.doranco.multitiers.dao.UserDAO;
import com.doranco.multitiers.entity.User;

@RunWith(Arquillian.class)
public class UserDAOTest {

	@Deployment
	public static JavaArchive createDeployment() {
		return ShrinkWrap.create(JavaArchive.class).addClasses(UserDAO.class, GenericDAO.class);
	}

	Logger logger = Logger.getLogger(UserDAOTest.class);
	@EJB
	private UserDAO dao;

	@Test
	@InSequence(1)
	public final void shouldCreateAUser() {
		
		
		for ( int i=1; i<25; i++) {
		User user = new User(); 
		user.setFirstName("lotus"+i);
		user.setLastName("test"+i);
		user.setUserName("pat"+i);
		user.setPasswrd("Mou"+i);
		
		try {
			dao.create(user);
			assertNotNull(user);

		} catch (Exception e) {
			fail(" ne devrait pas retourner d'erreur de creation");
		}
	}
	}
	//@Test
	@InSequence(2)
	public final void utilisateurExiste() {
		try {

			User connectedUser = dao.findByUserNameAndPasswrd("pat", "Mou");
			assertNotNull(connectedUser);

		} catch (Exception e) {
			logger.error("Error= ",e);
			fail("ne devrait pas retourner d'Exception lors de la recuperation d'un user avec les bon identifiants");
		}
	}

	//@Test
	@InSequence(3)
	public final void UtilisateurIntrouvable() {
		User user = null;
		try {

			user = dao.findByUserNameAndPasswrd("Patrick", "Moulin");

		} catch (Exception nre) {

		}
		assertNull(user);
	}
	
	@Test
	public final void FindALLUser () {
		List<User> listUser =null ;
		try {
			listUser=dao.FindALLUser ();
			 assertNotNull(listUser);
		}catch (Exception e){
			
		}
	}

	// @Test
	public final void testCreate() {
		fail("Not yet implemented"); // TODO
	}

	// @Test
	public final void testDelect() {
		fail("Not yet implemented"); // TODO
	}

	// @Test
	public final void testFind() {
		fail("Not yet implemented"); // TODO
	}

	// @Test
	public final void testUpdate() {
		fail("Not yet implemented"); // TODO
	}

}
