package com.doranco.multitiers.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.hibernate.Hibernate;

import com.doranco.multitiers.entity.User;
import com.doranco.multitiers.util.Page;

@Stateless
@LocalBean
public class UserDAO extends GenericDAO<User> {

	
String jpql=null;
	
		Logger logger=Logger.getLogger(UserDAO.class);

	public User findByUserNameAndPasswrd(String givenUserName, String givenPasswrd)throws NoResultException {

		User user;
		String jpql = "SELECT u FROM User u WHERE u.userName=:userName And u.passwrd=:passwrd";
		user=(User) em.createQuery(jpql).setParameter("userName", givenUserName).setParameter("passwrd", givenPasswrd)
				.getSingleResult();
		hibernateInitialiszation(user);
		return user;
	}
		public List<User> FindALLUser ()throws NoResultException {
		
		List<User> listUser ;
		 jpql = "SELECT u FROM User u";
		listUser=(List<User>)em.createQuery(jpql).getResultList();
		
		for (User user :  listUser) {
			hibernateInitialiszation(user);
		}
		return (listUser);
	}

	public User find(long id) throws Exception {
		User user = null;
		user = em.find(User.class, id);

		hibernateInitialiszation(user);

		return user;
	}

	private void hibernateInitialiszation(User user) {

		Hibernate.initialize(user.getNotes());
		Hibernate.initialize(user.getOrder());

	}

	public Page<User>find(int pageSize,int pageNumber){
			 
			Page<User> page= new Page<User>();
			jpql = "SELECT u FROM User u";
				
			Query query=em.createQuery(jpql);
			query.setMaxResults(pageSize);
			query.setFirstResult(pageNumber *pageSize +1);
				
				
// recuperation d'un page utilisateur
			List<User>pageContent=query.getResultList();
				
			//recuperation du nombre total de users
			int totalCount=(int)em.createQuery("SELECT count(u.id)FROM User u").getSingleResult();
				
				for (User user : pageContent) {
					hibernateInitialiszation(user);
				}
				page.setPageNumber(pageNumber);
				page.setPageSize(pageSize);
				page.setTotalCount(totalCount);
				page.setContent(pageContent);
				return (page);
				
		
		
	}

}
	
	

