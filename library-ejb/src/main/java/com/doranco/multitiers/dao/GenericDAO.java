package com.doranco.multitiers.dao;



import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import org.apache.log4j.Logger;

public class GenericDAO <T> {

	EntityManagerFactory emf=Persistence.createEntityManagerFactory("library");
	EntityManager em=emf.createEntityManager();
	public EntityTransaction transaction= em.getTransaction();

	private String jpql=null;
	Logger logger=Logger.getLogger (GenericDAO.class);

	public T create(T toBeCreated)throws Exception {


		transaction.begin();
		em.persist(toBeCreated);
		em.flush();
		transaction.commit();
		return toBeCreated;

	}

	public void delect (T toBeDeleted )throws Exception {
		transaction.begin();
		em.remove(toBeDeleted);
		transaction.commit();	
	}
	public T find(Class clazz,int primaryKey)throws Exception {
		return (T)em.find(clazz, primaryKey);

	}


	public T update(T toBeUpdated)throws Exception {

		transaction.begin();
		em.persist(toBeUpdated);;
		transaction.commit();
		return toBeUpdated;
	}
}
