package com.doranco.multitiers.dao;

import java.sql.DriverManager;
import java.sql.SQLException;

import javax.ejb.Stateless;

import org.apache.log4j.Logger;


@Stateless
public class ConnectionFactory {
	private static Logger logger=Logger.getLogger(ConnectionFactory.class);
	private java.sql.Connection connection;
	
	public ConnectionFactory() {
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
		}catch (ClassNotFoundException e) {
			logger.error("le pilote mysql n'est pas présent dans le classpath",e);
			
		}
		
		logger.debug(" pilote mysql enregistré");
		
		try {
		connection=DriverManager.getConnection("jdbc:mysql://localhost:3306/library","library","library");
		}catch (SQLException e) {
			logger.error("la connection a la base de données à échoué",e);
			
		}
		if(connection!=null)
			logger.debug(" connection mysql reussi");
		else
			logger.error(" probleme de connection à la base mysql");
		}

	public java.sql.Connection getConnection() {
		return connection;
	}
		
	

}
