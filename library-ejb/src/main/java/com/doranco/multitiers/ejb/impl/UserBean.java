package com.doranco.multitiers.ejb.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;

import org.apache.log4j.Logger;

import com.doranco.multitiers.dao.UserDAO;
import com.doranco.multitiers.ejb.interfaces.IUser;
import com.doranco.multitiers.ejb.interfaces.exceptions.LibraryExceptions;
import com.doranco.multitiers.entity.User;
import com.doranco.multitiers.util.Page;

@Stateless
public class UserBean  implements IUser{

	Logger logger= Logger.getLogger(UserBean.class);
	@EJB
	UserDAO userDAO;
	
	@Override
	public User suscribe(User user)throws LibraryExceptions {
		
		try {
			user= userDAO.create(user);
		}catch(Exception e) {
			logger.error("Problème survenu lors de l'inscription d'un utilisateur", e);
			throw new LibraryExceptions();
		}
		 return user;
	}

	@Override
	public User connect(String userName, String passwrd) throws LibraryExceptions {
		User user=null;
		try {
		
			user=userDAO.findByUserNameAndPasswrd (userName, passwrd);
				
			
		
		}catch (NoResultException nre){
			logger.error ("Impossible de trouver un tel utilisateur",nre);
		throw new LibraryExceptions();
		}catch(Exception e) {
			logger.error("Problème survenu lors de la connection d'un utilisateur", e);
			throw new LibraryExceptions();
		}
		
	return user;
	}

	@Override
	public List<User> getAll() throws LibraryExceptions {
		List<User> listUser=null;
		try {
		
			listUser=userDAO.FindALLUser ();
				
			
		
		}catch (NoResultException nre){
			logger.error ("Aucun utilisateur trouvé",nre);
		throw new LibraryExceptions();
		}catch(Exception e) {
			logger.error("Problème survenu lors de la requete d'affichage des utilisateurs", e);
			throw new LibraryExceptions();
		}
		
	return listUser;
	}

	@Override
	public User qetUser(long id) throws LibraryExceptions {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<User> find(int pagesize, int pageNumber) throws LibraryExceptions {
		// TODO Auto-generated method stub
		return null;
	}
}
