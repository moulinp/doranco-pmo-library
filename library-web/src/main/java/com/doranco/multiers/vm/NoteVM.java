package com.doranco.multiers.vm;

import java.io.Serializable;

public class NoteVM implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5022436225549464816L;
	private int value;
	private String comment;
	private long idBook;
	private long IdUser;
	
	

	public NoteVM() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public long getIdBook() {
		return idBook;
	}

	public void setIdBook(long idBook) {
		this.idBook = idBook;
	}

	public long getIdUser() {
		return IdUser;
	}

	public void setIdUser(long idUser) {
		IdUser = idUser;
	}
}
