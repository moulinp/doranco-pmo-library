package com.doranco.multiers.vm;

import java.io.Serializable;

public class BookVM implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8351646417886438559L;
	private long id;
	private String isbn;
	private String title;
	
	
	public BookVM() {
		super();
		// TODO Auto-generated constructor stub
	}


	public long getIdBook() {
		return id;
	}


	public void setIdBook(long idBook) {
		this.id = idBook;
	}


	public String getIsbn() {
		return isbn;
	}


	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}
	
	
}
