package com.doranco.multiers.vm;

import java.io.Serializable;
import java.util.List;

public class UserVM implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2128502738430598163L;
	private long id;
	private String firstName;
	private String lastName;
	private String userName;
	private boolean isAdmin;
	List<NoteVM> notes;
	List<OrderVM> orders;
	List<ViewingVM> viewings;

	public long getId() {
		return id;
	}


	public boolean isAdmin() {
		return isAdmin;
	}


	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public String getUserName() {
		return userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}


	public boolean isIsadmin() {
		return isadmin;
	}


	public void setIsadmin(boolean isadmin) {
		this.isadmin = isadmin;
	}


	private boolean isadmin = false;

}
