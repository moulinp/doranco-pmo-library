package com.doranco.multiers.vm;

import java.io.Serializable;
import java.util.Date;

public class CredentialVM implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5244528499829590227L;
	private long idBook;
	private Date startDate;
	private long duration;
	
	

	public CredentialVM() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public long getDuration() {
		return duration;
	}

	public void setDuration(long duration) {
		this.duration = duration;
	}

	public long getIdBook() {
		return idBook;
	}

	public void setIdBook(long idBook) {
		this.idBook = idBook;
	}

}
