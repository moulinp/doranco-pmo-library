package com.doranco.multiers.vm;

import java.io.Serializable;

public class OrderLineVM implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1611705514487336414L;
	private long idBook;

	public OrderLineVM() {
		super();
		// TODO Auto-generated constructor stub
	}

	public long getIdBook() {
		return idBook;
	}

	public void setIdBook(long idBook) {
		this.idBook = idBook;
	}

}
