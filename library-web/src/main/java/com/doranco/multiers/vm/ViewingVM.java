package com.doranco.multiers.vm;

import java.io.Serializable;

public class ViewingVM implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5131326129288062134L;
	private long idBook;

	public ViewingVM() {
		super();
		// TODO Auto-generated constructor stub
	}

	public long getIdBook() {
		return idBook;
	}

	public void setIdBook(long idBook) {
		this.idBook = idBook;
	}

}
