package com.doranco.multitiers.mappers;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import com.doranco.multiers.vm.ViewingVM;
import com.doranco.multitiers.entity.Viewing;


@Mapper
public interface ViewingMapper {
	
	ViewingMapper INSTANCE  = Mappers.getMapper(ViewingMapper.class);
	
	@Mapping(source="book.id", target="idBook")
	ViewingVM entityToVM(Viewing entity);
	
	
	List<ViewingVM> entitiesToVMs(List<Viewing> entities);

}
