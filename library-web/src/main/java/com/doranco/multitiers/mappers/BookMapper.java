package com.doranco.multitiers.mappers;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.doranco.multiers.vm.BookVM;
import com.doranco.multitiers.entity.Book;


@Mapper
public interface BookMapper {

	BookMapper INSTANCE = Mappers.getMapper(BookMapper.class);

	BookVM entityToVM(Book entity);
	
	List<BookVM> entitiesToVMs(List<Book> entities);
}
