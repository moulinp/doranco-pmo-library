package com.doranco.multitiers.mappers;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.doranco.multiers.vm.UserVM;
import com.doranco.multitiers.entity.User;
import com.doranco.multitiers.util.Page;

@Mapper(uses= {NoteMapper.class,OrderMapper.class,ViewingMapper.class})
public interface UserMapper {

	UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);
	
	UserVM entityToVM(User user);
	
	List<UserVM> entitiesToVMs(List<User> user);
	Page<UserVM> entitiesPageToVMsPage(Page<User>page);
	
	
}
