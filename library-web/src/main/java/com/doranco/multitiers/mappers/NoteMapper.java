package com.doranco.multitiers.mappers;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.doranco.multiers.vm.NoteVM;
import com.doranco.multitiers.entity.Note;



@Mapper
public interface NoteMapper {

	NoteMapper INSTANCE = Mappers.getMapper(NoteMapper.class);

	@Mappings({ 
		@Mapping(source = "book.id", target = "idBook"), 
		@Mapping(source = "user.id", target = "idUser") })
	NoteVM entityToVM(Note entity);
	
	
	List<NoteVM> entitiesToVMs(List<Note> entities);

}
