package com.doranco.multitiers.resource;

import java.util.List;

import javax.naming.NamingException;
import javax.websocket.server.PathParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.doranco.multiers.vm.UserVM;
import com.doranco.multitiers.RemoteContext;
import com.doranco.multitiers.ejb.interfaces.IUser;
import com.doranco.multitiers.ejb.interfaces.exceptions.LibraryExceptions;
import com.doranco.multitiers.entity.User;
import com.doranco.multitiers.mappers.UserMapper;
import com.doranco.multitiers.util.Page;




@Path("users")
@Produces(MediaType.APPLICATION_JSON)
public class UserResources {

	// @EJB(lookup="java:global/library-ear/library-ejb/UserBean")
	IUser iUser;

	UserMapper userMapper = UserMapper.INSTANCE;

	Logger logger = Logger.getLogger(UserResources.class);

	public UserResources() throws LibraryExceptions {
		super();
		try {
			iUser = (IUser) RemoteContext.getInstance().getInitialContext()
					.lookup("java:global/library-ear/library-ejb/UserBean");
		} catch (NamingException e) {
			logger.error("Impossible de démarrer la ressource UserResource", e);
			throw new LibraryExceptions();
		}

	}

	@POST
	public User suscribe(User user) {

		try {
			user = iUser.suscribe(user);
		} catch (LibraryExceptions e) {
			logger.error("Impossible de démarrer la ressource UserResource", e);
			// TODO Send a custom message to the user
		}

		return user;

	}

	@Path("{id}")
	@GET	
	public UserVM getUser(@PathParam("id") long id) {
			UserVM userVM = null;
		
		logger.debug("Trying to get a user form its id =="+id);

		try {
			userVM = userMapper.entityToVM(iUser.qetUser(id));
		} catch (LibraryExceptions e) {
			logger.error("Impossible de recup un user", e);
			// TODO Send a custom message to the user
		}

		return userVM;
	}
	

	@GET	
	public Page<UserVM> getUsers(@QueryParam("pageSize")int pageSize,@QueryParam("pageNHumber")int pageNumber){

		Page<UserVM> userVMs = null;
		try {
			userVMs = userMapper.entitiesPageToVMsPage(iUser.find(pageSize, pageNumber));
		
		}catch (Exception e) {
			
			logger.error("Problème lors de la récupération des users", e);
			// TODO Send a custom message to the user
		}
		return userVMs;

	}

}

